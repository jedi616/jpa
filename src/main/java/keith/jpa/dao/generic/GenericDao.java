package keith.jpa.dao.generic;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author Keith F. Jayme
 *
 */
public interface GenericDao<E, ID extends Serializable>
{
    List<E> findAll() throws Exception;

    List<E> findAll(String jpqlQuery, Map<String, Object> params) throws Exception;

    E findOne(ID id) throws Exception;

    E save(E entity) throws Exception;

    void delete(ID id) throws Exception;

    void truncateTable() throws Exception;
}
