package keith.jpa.dao.generic;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Table;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import keith.jpa.rawdao.ConnectionUtil;

/**
 * 
 * @author Keith F. Jayme
 *
 */
public abstract class GenericDaoImpl<E, ID extends Serializable> implements GenericDao<E, ID>
{
    private Class<E> entityClass;

    @SuppressWarnings("unchecked")
    public GenericDaoImpl()
    {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        entityClass = (Class<E>) genericSuperclass.getActualTypeArguments()[0];
    }

    @Override
    public List<E> findAll() throws Exception
    {
        EntityManager entityManager = ConnectionUtil.createEntityManager();
        try
        {
            CriteriaQuery<E> criteriaQuery = entityManager.getCriteriaBuilder().createQuery(entityClass);
            Root<E> root = criteriaQuery.from(entityClass);
            criteriaQuery.select(root);
            return entityManager.createQuery(criteriaQuery).getResultList();
        }
        finally
        {
            entityManager.close();
        }
    }

    @Override
    public List<E> findAll(String jpqlQuery, Map<String, Object> params) throws Exception
    {
        EntityManager entityManager = ConnectionUtil.createEntityManager();
        try
        {
            TypedQuery<E> query = entityManager.createQuery(jpqlQuery, entityClass);
            for (Map.Entry<String, Object> entry : params.entrySet())
            {
                String key = entry.getKey();
                Object value = entry.getValue();
                query.setParameter(key, value);
            }
            return query.getResultList();
        }
        finally
        {
            entityManager.close();
        }
    }

    @Override
    public E findOne(ID id) throws Exception
    {
        EntityManager entityManager = ConnectionUtil.createEntityManager();
        try
        {
            return entityManager.find(entityClass, id);
        }
        finally
        {
            entityManager.close();
        }
    }

    @Override
    public E save(E entity) throws Exception
    {
        EntityManager entityManager = ConnectionUtil.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try
        {
            transaction.begin();
            E savedEntity = entityManager.merge(entity);
            transaction.commit();
            return savedEntity;
        }
        catch (Exception e)
        {
            transaction.rollback();
            throw e;
        }
        finally
        {
            entityManager.close();
        }
    }

    @Override
    public void delete(ID id) throws Exception
    {
        EntityManager entityManager = ConnectionUtil.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try
        {
            transaction.begin();
            E entityToDelete = entityManager.find(entityClass, id);
            entityManager.remove(entityToDelete);
            transaction.commit();
        }
        catch (Exception e)
        {
            transaction.rollback();
            throw e;
        }
        finally
        {
            entityManager.close();
        }
    }

    @Override
    public void truncateTable() throws Exception
    {
        System.out.println("GENERIC:");
        EntityManager entityManager = ConnectionUtil.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try
        {
            transaction.begin();
            entityManager.createNativeQuery("SET FOREIGN_KEY_CHECKS = 0").executeUpdate();

            Table tableAnnotation = entityClass.getAnnotation(Table.class);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("truncate table ");
            stringBuilder.append(tableAnnotation.name());
            entityManager.createNativeQuery(stringBuilder.toString()).executeUpdate();

            entityManager.createNativeQuery("SET FOREIGN_KEY_CHECKS = 1").executeUpdate();
            transaction.commit();
        }
        catch (Exception e)
        {
            transaction.rollback();
            throw e;
        }
        finally
        {
            entityManager.close();
        }
    }
}
