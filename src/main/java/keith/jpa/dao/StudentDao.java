package keith.jpa.dao;

import keith.domain.jpa.entity.StudentM;
import keith.jpa.dao.generic.GenericDao;

/**
 * 
 * @author Keith F. Jayme
 *
 */
public interface StudentDao extends GenericDao<StudentM, Long>
{
}
