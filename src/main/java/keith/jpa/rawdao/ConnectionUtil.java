package keith.jpa.rawdao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * 
 * @author Keith F. Jayme
 *
 */
public class ConnectionUtil
{
    protected static EntityManagerFactory entityManagerFactory;

    static
    {
        entityManagerFactory = Persistence.createEntityManagerFactory("jpa-project");
    }

    public static EntityManager createEntityManager()
    {
        return entityManagerFactory.createEntityManager();
    }
}
