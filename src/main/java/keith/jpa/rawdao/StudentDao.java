package keith.jpa.rawdao;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import keith.domain.jpa.entity.StudentM;

/**
 * 
 * @author Keith F. Jayme
 *
 */
public class StudentDao
{
    private static StudentDao studentDao;

    static
    {
        studentDao = new StudentDao();
    }

    public static StudentDao getInstance()
    {
        return studentDao;
    }

    public List<StudentM> findAll() throws Exception
    {
        EntityManager entityManager = ConnectionUtil.createEntityManager();
        try
        {
            CriteriaQuery<StudentM> criteriaQuery = entityManager.getCriteriaBuilder().createQuery(StudentM.class);
            Root<StudentM> root = criteriaQuery.from(StudentM.class);
            criteriaQuery.select(root);
            return entityManager.createQuery(criteriaQuery).getResultList();
        }
        finally
        {
            entityManager.close();
        }
    }

    public List<StudentM> findAll(String jpqlQuery, Map<String, Object> params) throws Exception
    {
        EntityManager entityManager = ConnectionUtil.createEntityManager();
        try
        {
            TypedQuery<StudentM> query = entityManager.createQuery(jpqlQuery, StudentM.class);
            for (Map.Entry<String, Object> entry : params.entrySet())
            {
                String key = entry.getKey();
                Object value = entry.getValue();
                query.setParameter(key, value);
            }
            return query.getResultList();
        }
        finally
        {
            entityManager.close();
        }
    }

    public StudentM findOne(Long studentId) throws Exception
    {
        EntityManager entityManager = ConnectionUtil.createEntityManager();
        try
        {
            return entityManager.find(StudentM.class, studentId);
        }
        finally
        {
            entityManager.close();
        }
    }

    public StudentM save(StudentM student) throws Exception
    {
        EntityManager entityManager = ConnectionUtil.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try
        {
            transaction.begin();
            StudentM savedStudent = entityManager.merge(student);
            transaction.commit();
            return savedStudent;
        }
        catch (Exception e)
        {
            transaction.rollback();
            throw e;
        }
        finally
        {
            entityManager.close();
        }
    }

    public void delete(Long studentId) throws Exception
    {
        EntityManager entityManager = ConnectionUtil.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try
        {
            transaction.begin();
            StudentM studentToDelete = entityManager.find(StudentM.class, studentId);
            entityManager.remove(studentToDelete);
            transaction.commit();
        }
        catch (Exception e)
        {
            transaction.rollback();
            throw e;
        }
        finally
        {
            entityManager.close();
        }
    }

    public void truncateTable() throws Exception
    {
        EntityManager entityManager = ConnectionUtil.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try
        {
            transaction.begin();
            entityManager.createNativeQuery("SET FOREIGN_KEY_CHECKS = 0").executeUpdate();
            entityManager.createNativeQuery("truncate table student_m").executeUpdate();
            entityManager.createNativeQuery("SET FOREIGN_KEY_CHECKS = 1").executeUpdate();
            transaction.commit();
        }
        catch (Exception e)
        {
            transaction.rollback();
            throw e;
        }
        finally
        {
            entityManager.close();
        }
    }
}
